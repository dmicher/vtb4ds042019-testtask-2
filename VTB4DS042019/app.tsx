﻿declare var require: any

var React = require('react');
var ReactDOM = require('react-dom');

class App extends React.Component {
    // конструктор компонента
    constructor(props) {
        super(props);
        this.state = {
            items: Array,
            selected: "-= показать все =-",
            isLoaded: false,
        }
    }
    // Вызываем загрузку компонента перед рендерингом (данные подъедут немного позже первой отрисовки)
    async componentDidMount() {
        if (!this.state.isLoaded) {
            fetch('http://vtb4ds042019.somee.com/get')
                .then((response) => response.json())
                .then((response) => {
                    this.setState({ items: response });
                    this.setState({ isLoaded: true });
                })
                .then((error) => { this.setState({ error }); })
        }
    }
    // реагируем на изменение выбора валюты, принудительно перезагружая компонент с новым фильтром
    selectChangeHandler = e => {
        var selID = (document.getElementById("vselect") as HTMLSelectElement).options.selectedIndex;
        this.state.selected = (document.getElementById("vselect") as HTMLSelectElement).options[selID].value.toString();
        this.forceUpdate();
    }
    // отрисовываем компонент
    render() {
        // если данные из API подъехали, формируем из них строки таблицы и варианты вабора для фильтра валют
        if (this.state.isLoaded) {
            // курсы валют
            var filtered = this.state.items.filter(function (item) {
                return (this.state.selected === item.Vname || this.state.selected === "-= показать все =-");
            }, this);
            var list = filtered.map(function (item, index) {
                return (
                    <tr key={index} >
                        <td>{item.Vname}</td>
                        <td align='center'>{item.Vcurs.toFixed(4)}</td>
                        <td align='center'>{(1 / item.Vcurs).toFixed(4)}</td>
                    </tr>
                )
            });
            // элементы фильтра
            var opt = this.state.items.map(function (item, index) {
                return <option key={index} value={item.Vname}>{item.Vname}</option>;
            });
        }
        // Внешний вид
        return (
            <div>
                <h1>Тестовое задание VTB4DS042019</h1>
                <h3>Информация</h3>
                <div>
                    <p> Заказчик: %РАБОТОДАТЕЛЬ%
                    <br /> Исполнитель: Черкасов Д.С.
                    <br /> Данный web-сайт предоставляет значения курсов валют на сегодня от ЦБ РФ. Работает на API2, react js, данные хранит в собственной базе данных на этом же сайте.
                    <br /> Время разработки: план 05-12 апреля, факт 05-15 апреля 2019 года.
                    <br /> Сегодня: {new Date().toLocaleDateString()} </p>
                </div>
                <hr/>
                <h3>Курсы валют по данным Центрального банка Российской Федерации</h3>
                <div>
                    <table border='1' bordercolor='000' cellPadding='5' cellSpacing='0'>
                        <thead>
                            <tr>
                                <th rowSpan='2'>Валюта<br/>
                                        <select id='vselect' onChange={this.selectChangeHandler}>
                                            <option value="-= показать все =-">-= показать все =-</option>
                                        {opt}
                                    </select>
                                </th>
                                <th colSpan='2'>Курс валюты</th>
                            </tr>
                            <tr>
                                <th>валюта к рублю</th>
                                <th>рубль к валюте</th>
                            </tr>
                        </thead>
                        <tbody>{ list }</tbody>
                    </table>
                </div>
                <hr/>
                <div align='right'>dmicher abathur kubrow,<br /> апрель 2019</div>
            </div>
        );
    }

}

ReactDOM.render(<App />, document.getElementById('root'));