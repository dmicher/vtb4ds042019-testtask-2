CREATE TABLE [dbo].[ValuteCursOnDate_VTB4DS042019] (
    [ID]    INT            IDENTITY (0, 1) NOT NULL,
    [Vname] NVARCHAR (MAX) NOT NULL,
    [Vcurs] FLOAT (53)     NOT NULL,
    [Vdate] DATE           NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

