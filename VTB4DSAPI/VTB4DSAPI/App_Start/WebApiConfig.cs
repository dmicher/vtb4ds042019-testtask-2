﻿using System.Net.Http.Formatting;
using System.Web.Http;

namespace VTB4DSAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Конфигурация и службы веб-API
            config.EnableCors();

            // Маршруты веб-API
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Оставляем только форматер для JSON
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());

        }
    }
}
