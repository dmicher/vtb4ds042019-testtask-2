﻿using System;
using System.Data;
using System.Data.SqlClient;
using TestTask.CBRF;

namespace TestTask.Models
{
    /// <summary>
    /// Класс, позволяющий получить данные о курсах валют, установленных ЦБ РФ на сегодня.
    /// Класс запрашивает даные ЦБ РФ не чаще чем 1 раз в день и хранит данные в базе данных MS SQL,
    /// к которой подключается по передаваемой классу строке подвлючения.
    /// </summary>
    public class ValuteCursGetter
    {
        /// <summary>
        /// Открытый метод, возвращает данные в виде таблицы (DataTable) о курсах валют на сегодня.
        /// Смотрит сначала в базу данных по представленной строке подключения (если нужно её инициализирует);
        /// если данных там не нашёл, лезет в сервис ЦБ РФ, берёт данные оттуда и добавляет в базу данных.
        /// </summary>
        /// <param name="connectionStr">Строка подключения к базе данных</param>
        /// <param name="dataOut">Исходящий параметр с данными (DataTable) для запрашивающего кода</param>
        /// <returns>Возвращает результат загрузки данных: "Правда" - успех, "Ложь" - неудача</returns>
        public bool GetCursDataTable (string connectionStr, out DataTable dataOut)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionStr))
                {
                    connection.Open();
                    // Смотрим, есть ли в базе данных таблица "ValuteCursOnDate_VTB4DS042019". Если её там нет, создаём
                    if (new SqlCommand("SELECT OBJECT_ID (N'ValuteCursOnDate_VTB4DS042019')", connection).ExecuteScalar() is DBNull)
                    {
                        string sqlStr = "CREATE TABLE [dbo].[ValuteCursOnDate_VTB4DS042019] (" +
                                        "[ID] INT NOT NULL PRIMARY KEY IDENTITY(0, 1), " +
                                        "[Vname] NVARCHAR(MAX) NOT NULL," +
                                        "[Vcurs] FLOAT NOT NULL, " +
                                        "[Vdate] DATE NOT NULL )";
                        new SqlCommand(sqlStr, connection).ExecuteNonQuery();
                    }
                    // Заглядываем в базу данных: если данные выбрать не удалось, либо на сегодня данных нет
                    // запрашиваем их у ЦБ РФ. Если и там неудача - кидаем исключение
                    if (!GetCursesFromMSSQL(connectionStr, out dataOut) || dataOut.Rows.Count == 0)
                    {
                        if (!GetCursesFromCBRF(out dataOut)) throw new Exception();
                        else
                        {
                            // При успешном получении данных из ЦБ РФ записываем их в базу данных, предварительно обработав
                            SqlCommand cmd;
                            foreach (DataRow dr in dataOut.Rows)
                            {
                                string tName = dr["Vname"].ToString().Replace("\"", "");
                                // от ЦБ РФ (по состоянию на 06.04.2019) приходит строка и длинным хвостом из пробелов - удаляем
                                for (int i = 0; i < tName.Length; ++i)
                                {
                                    if (tName[i] == ' ' && tName[i + 1] == ' ')
                                    {
                                        tName = tName.Remove(i);
                                        break;
                                    }
                                }
                                // от ЦБ РФ приходит строка с разделителем дробной части в виде запятой (не примет БД) - меняем
                                string tCurs = dr["Vcurs"].ToString().Replace(',', '.');
                                // SQL запрос на добавление компонентов в БД
                                cmd = new SqlCommand("INSERT INTO [dbo].[ValuteCursOnDate_VTB4DS042019](Vname, Vcurs, Vdate)" +
                                                     $" VALUES (@name, @curs, '{DateTime.Now.ToString("yyyy-MM-dd")}'); " +
                                                     "SET @ID = @@IDENTITY;", connection);
                                cmd.Parameters.AddWithValue("@name", tName);
                                cmd.Parameters.AddWithValue("@curs", tCurs);
                                SqlParameter idParam = cmd.Parameters.Add("@ID", SqlDbType.Int, 0, "ID");
                                idParam.Direction = ParameterDirection.Output;
                                cmd.ExecuteNonQuery();
                            }
                        }
                        // Для удобства возьмём нормализованные данные из БД (вместо "грязных" данных из ЦБ РФ)
                        if (!GetCursesFromMSSQL(connectionStr, out dataOut) || dataOut.Rows.Count == 0)
                            throw new Exception("Только что записанные данные в БД не возможно прочесть");
                    }
                    // В случае удачного получения данных возвращаем "Правду"
                    return true;
                }
            }
            catch (Exception)
            {
                // при ошибке возвращаем "Ложь" и пустую таблицу данных
                dataOut = null;
                return false;
            }
        }
        /// <summary>
        /// Метод получает из сервиса ЦБ РФ данные по курсам валют на сегодня.
        /// </summary>
        /// <param name="output">Таблица, подлежащая заполнению данными из ЦБ РФ</param>
        /// <returns>Возвращает результат загрузки данных: "Правда" - успех, "Ложь" - неудача</returns>
        private bool GetCursesFromCBRF(out DataTable output)
        {
            try
            {
                // Получаем данные у ЦБ РФ
                DataTable CBRFCurseTable = new DailyInfoSoapClient().GetCursOnDate(DateTime.Now).Tables["ValuteCursOnDate"];

                // Удаляем из таблицы не нужные для работы приложения столбцы (ЦБ РФ 06.04.2019)
                //   Vname   - Название валюты           (нужно)
                //   Vnom    - Номинал                   (---)
                //   Vcurs   - Курс                      (нужно)
                //   Vcode   - ISO Цифровой код валюты   (---)
                //   VchCode - ISO Символьный код валюты (---)
                CBRFCurseTable.Columns.Remove("Vnom");
                CBRFCurseTable.Columns.Remove("Vcode");
                CBRFCurseTable.Columns.Remove("VchCode");

                // Полученную информацию кладём в представленную ссылку и возвращаеми "ПРАВДА" в знак удачного завершения
                output = CBRFCurseTable;
                return true;
            }
            catch (Exception)
            {
                // При неудаче возвращаем ЛОЖЬ и пустую таблицу данных
                output = null;
                return false;
            }
        }
        /// <summary>
        /// Метод получает из базы данных по переданной строке подключения данные по курсам валют на сегодня.
        /// Предполагается, что БД по строке подключения имеет соответствующую таблицу с данными (не проверяется).
        /// </summary>
        /// <param name="connectionStr">Строка подключения к базе данных</param>
        /// <param name="output">Таблица, подлежащая заполнению из базы данных</param>
        /// <returns>Возвращает результат загрузки данных: "Правда" - успех, "Ложь" - неудача</returns>
        private bool GetCursesFromMSSQL(string connectionStr, out DataTable output)
        {
            try
            {
                DataTable DBCurseTable = new DataTable();
                // Получаем данные и БД: "Названиа", "курс" на сегодня
                string sqlStr = $"SELECT [Vname], [Vcurs] FROM [ValuteCursOnDate_VTB4DS042019] WHERE [Vdate] = '{DateTime.Now.ToString("yyyy-MM-dd")}'";
                using (SqlConnection connection = new SqlConnection(connectionStr))
                {
                    connection.Open();
                    DBCurseTable.Load(new SqlCommand(sqlStr, connection).ExecuteReader());
                }
                // Если данные не вытянулись, кидаем исключение, иначе данные передаём их в output и выходим по true
                output = DBCurseTable ?? throw new Exception("Получить данные из внутренней БД не удалось.");
                return true;
            }
            catch (Exception)
            {
                // В любой непонятной ситуации в output кладём null и выходим по false
                output = null;
                return false;
            }
        }
    }
}