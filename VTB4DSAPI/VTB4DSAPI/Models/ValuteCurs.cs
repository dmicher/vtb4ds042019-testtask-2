﻿using System;

namespace TestTask.Models
{
    [Serializable]
    /// <summary>
    /// Класс отражающий курсы конкретной валюты
    /// </summary>
    public class ValuteCurs
    {
        /// <summary>
        /// Получает или задаёт название валюты
        /// </summary>
        public string Vname { get; set; }
        /// <summary>
        /// Получает или задаёт текущий курс валюты по отношению к рублю
        /// </summary>
        public float Vcurs { get; set; }
        /// <summary>
        /// Конструктор задаёт название и курс валюты к рублю
        /// </summary>
        /// <param name="name">Наименование валюты</param>
        /// <param name="curs">Курс к рублю</param>
        public ValuteCurs(string name, float curs)
        {
            Vname = name;
            Vcurs = curs;
        }
    }
}