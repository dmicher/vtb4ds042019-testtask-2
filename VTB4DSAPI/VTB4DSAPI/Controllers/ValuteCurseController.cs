﻿using System;
using System.Data;
using TestTask.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace TestTask.Controllers
{
    /// <summary>
    /// Контроллер, предоставляющий запрашиваемому коду информацию по текущему курсу валют
    /// </summary>
    [EnableCors(origins: "http://vtb4ds042019.somee.com", headers: "*", methods: "*")] // API разрешаем пользоваться только своему сайту
    public class ValuteCurseController : ApiController
    {
        /// <summary>
        /// Строка подключения к базе данных на сайте https://somee.com
        /// </summary>
        private static readonly string connectionString = "workstation id=vtb4ds042019.mssql.somee.com;" +
                                                          "packet size=4096;" +
                                                          "user id=dmicher_SQLLogin_1;" +
                                                          "pwd=a9jfwnl4fu;" +
                                                          "data source=vtb4ds042019.mssql.somee.com;" +
                                                          "persist security info=False;" +
                                                          "initial catalog=vtb4ds042019";
        /// <summary>
        /// Перечень курсов валют
        /// </summary>
        private List<ValuteCurs> vCursesDataList;
        /// <summary>
        /// Дата последнего удачного запроса а API
        /// </summary>
        private DateTime lastRequestDate;
        [Route("get")]
        [HttpGet]
        /// <summary>
        /// Возвращает запрашиваемые курсы валют на сегодня в формате JSON [id:{Vname: "", Vcurs: 0}, ...]
        /// </summary>
        /// <returns>Массив объектов JSON с курсами валют на сегодня</returns>
        public JObject[] GetCurses ()
        {
            // Если сегодня ещё не запрашивался курс валют, то обновляем перечень курсов валют
            if (DateTime.Today != lastRequestDate) UpdateDataList();
            // сериализуем исходящий массив данных
            JObject[] output = new JObject[vCursesDataList.Count];
            for (int i = 0; i < vCursesDataList.Count; ++i)
                output[i] = JObject.Parse(JsonConvert.SerializeObject(vCursesDataList[i]));
            return output;
        }
        /// <summary>
        /// Обновляет перечень курсов валют на сегодня из собственной базы данных или ЦБ РФ
        /// </summary>
        /// <param name="connectionStr">Строка подключения к базе данных SQL</param>
        private void UpdateDataList ()
        {
            // обнуляем (инициализируем) перечисление 
            if (vCursesDataList == null) vCursesDataList = new List<ValuteCurs>();
            else vCursesDataList.Clear();
            // Пробуем получить данные
            if (!new ValuteCursGetter().GetCursDataTable(connectionString, out DataTable table))
            {
                // Если данные не получены - ставим невероятную дату, чтобы при следующем запросе попытаться вновь загрузить данные
                lastRequestDate = DateTime.MinValue.Date;
                return;
            }
            else
            {
                // Наполняем перечень полученными данными
                foreach (DataRow dr in table.Rows)
                    vCursesDataList.Add(new ValuteCurs(dr["Vname"].ToString(), float.Parse(dr["Vcurs"].ToString())));
            }
            // обновляем дату последнего обращения
            lastRequestDate = DateTime.Today;
        }
    }
}
